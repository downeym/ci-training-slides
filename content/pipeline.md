## Pipeline
![Pipelines](https://docs.gitlab.com/ce/ci/img/pipelines.png)

- A pipeline is a group of jobs that get executed in stages.

-  All of the jobs in a stage are executed in parallel (if there are enough concurrent Runners)

- If one of the jobs fails, the next stage is not (usually) executed
