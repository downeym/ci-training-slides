## Intro App

- Project objective: Setup a public site

- Workflow description and project setup
  - Protect `master` (no push, master can merge)
  - Shared Runners
  - Issues, MR and Branching

----------

## Prep
- Create a group

- Create a Project

- Add README to initiate the project

- Add issues for CI, index and content

----------

## Development

- Branch, work and commit online

- Create a Merge Request

- Track Pipelines

- View updates through Pages and CI
