## CI/CD Architecture

![CI Architecture](https://about.gitlab.com/images/ci/arch-1.jpg)

- GitLab Runners can be installed locally or on remote servers

----------

- Specific and Shared

- Runners have an auto scaling option

- Runners support Docker and Bash

- One Runner installation can register multiple Runners
